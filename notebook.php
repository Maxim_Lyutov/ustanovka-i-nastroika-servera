<?php
    //notebook
    $data_json = file_get_contents('./notebook.json', FILE_USE_INCLUDE_PATH );
    $notebook = json_decode($data_json, true);
    //print_r($data_json);
    //print_r($notebook); 
?>


<!DOCTYPE html>
<html lang="ru">
  <head>
      <meta charset="utf-8">
      <title>Задание №2</title>
      <style>
          body {
              font-family: sans-serif;
          }
           h2 {
             margin: 0 0 0 1em;
           }
           p {
               margin: 0 0 0 3em;
               line-height: 2;
               font-size: 1.1em;
            }
           table {
                width: 75%;
                margin: 15px auto;
           }
      </style>
  </head>
  <body>
     <h2>Домашнее задание к лекции 2.1 </h1>
     <table>
        <tr>
           <td> Номер </td>
           <td> Имя </td>
           <td> Фамилия </td>
           <td> Адрес </td>
           <td> Телефон </td>
        </tr>
 <?php foreach ($notebook as $i => $people) : ?>
        <tr>
            <td> <?=$i+1?> </td>
            <td> <?php echo $people['firstName']?> </td>
            <td> <?php echo $people['lastName']?> </td>
            <td> <?php echo $people['address']?> </td>
            <td> <?php echo $people['phoneNumber']?> </td>
         </tr>
<?php endforeach; ?>

     </table>
  </body>
</html>
